const { assert, expect } = require('chai');
const { describe, it } = require('mocha');

const map = require('./../functions/map')

module.exports = describe('#2 - Recoder la fonction map', function () {
  
  it('return a new array', function () {
    expect(map([1, 2, 3, 4], function (number) {
      return number * 2;
    })).to.eqls([2, 4, 6, 8])

    expect(map([1, 2, 3, 4], function (number) {
      return number / 2;
    })).to.eqls([0.5, 1, 1.5, 2])

    expect(map(['test', 'jy', 'oui', 'argent'], function (string) {
      return `kek ${string}`;
    })).to.eqls(['kek test', 'kek jy', 'kek oui', 'kek argent'])
  });
  
  it('should return false', function () {
    assert.equal(map([], null), false)
    assert.equal(map(null, undefined), false)
    assert.equal(map(undefined, null), false)
  });

});