const { assert, expect } = require('chai');
const { describe, it } = require('mocha');

const flatten = require('./../functions/flatten')

module.exports = describe('#1 - Créer une fonction qui met à plat plusieurs tableaux', function () {

  it('return a new array', function () {
    expect(flatten([1, [2], [3, 4, [5], [6, [7, [8]]]]])).to.eqls([1, 2, 3, 4, 5, 6, 7 ,8])
    expect(flatten([1, [2], [3, 4, [5]]])).to.eqls([1, 2, 3, 4, 5])
  });

  it('should return false', function () {
    assert.equal(flatten([]), false)
    assert.equal(flatten(null), false)
    assert.equal(flatten(undefined), false)
  });
  
});