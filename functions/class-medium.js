class Vehicule {
  constructor(model, price){
    this.model = model
    this.price = price

  }
  toString() {
    return 'Model: ' + this.model + ', Price: ' + this.price
  }
}

class Trucks extends Vehicule{
  constructor (model, price){
  super(model, price)
  }

  start() {
    return 'Le camion démarre'
  }

  speedUp(){
    return 'Le camion accélére'
  }
}

class Car extends Vehicule{
  constructor (model, price){
    super(model, price)
  }

  start() {
    return 'La voiture démarre'
  }

  speedUp(){
    return 'La voiture accélére'
  }
}





module.exports = Trucks
module.exports = Car
module.exports = Vehicule