function objectKeys(object) {
  
  if(!object ||  (typeof object[0] === 'string')) return false
  let tab = []
  for(i = 0; i<Object.keys(object).length ; i++){
    // console.log(Object.keys(object)[i])
    tab.push(Object.keys(object)[i])
  }
  return tab

}
// objectKeys( { firstName: 'John', lastName: 'Legend', age: 20 })


module.exports = objectKeys
