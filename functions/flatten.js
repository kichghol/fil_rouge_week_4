function flatten(arr) {
  if(!arr)return false
  let flatArray = []
  arr.forEach(element => {
    if(Array.isArray(element)){
      flatArray = flatArray.concat(flatten(element));
    }else {
      flatArray.push(element);
    }
  })

  return flatArray
}
flatten([1, [2], [3, 4, [5], [6, [7, [8]]]]])



// if(!myArray) return false
  
//   newArray = []

//   for (let i = 0; i<myArray.length; i++){
//     if (Array.isArray(myArray[i])){
//       newArray = newArray.concat(flatten(myArray[i]))
//       continue

//     }
//     newArray.push(myArray[i])

//   }
//   return newArray
// }
module.exports = flatten