String.prototype.truncate = function(position) {
  let str = this + ""
  let points = "..."
  if(str.length <= position) return str
  if(str[position-1] === " " ||str[position-1] === "," ) {  //revient une position avant si , ou espace
    str = str.substring(0,position-1)
    str = str + points
    return str
  }

  str = str.substring(0,position)
  str = str + points
  return str
}



module.exports = String.prototype.truncate