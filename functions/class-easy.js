class User {
  constructor (firstName, lastName) {
    this.firstName = firstName
    this.lastName = lastName
  }

  spellName() {
    return 'My name is' +'  ' + this.firstName + ' ' +  this.lastName
  }
}

module.exports = User